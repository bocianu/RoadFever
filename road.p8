pico-8 cartridge // http://www.pico-8.com
version 8
__lua__
-- road game by bocianu
-- camera & view properties
fps=30
height=127 width=127
drawdist=50
camdep=3.2
camh=100

-- road properties
segments={}
seglength=200 -- circa 185 meters
maxy=0

-- player & car properties
player={x=0,z=400}
playerprev=1
position={seg=99,z=0,chunk=1}
speed=0
maxspeed=280
accel=1
brake=-4
decel=-1
rumbledecel=-5
centrifugal=.4
braking=0
turning=0
gears={20,50,90,140,200,280}
collided=0

ulevel = 0
ulaps = 3
umode = 0
menupos = 0

-- cars
cars={}

-- const
st_title = 0
st_countdown = 1
st_race = 2

levels={
		{                       -- level 1
      name="80's valley",
			roadwidth=150,
			lanes=3,
			rumblelen=1,
			colors={road=5,finish=6,rumble={7,8},lanes={6,5},offroad=3,
              sky={12,12,12,12,12,14,15}},
      sun={x=63,y=63,r=24,col=9},
      plane2={s=128,w=16,h=16,y=-8,bg=5},
			track={
				{{100,0,-6}},            --    len,[curve],[hill]
				{{80,1},{40,-2}},   -- or lenin,lenmid,lenout,[curve],[hill]
				{{50,-2}},
				{{100,3,0}},
				{{100,-2,0}},
				{{50,4,2}},
				{{50,-4,-2}},
        {{40,0,3},{60,-2,-2}},
        {{40},{30,4,0}}
			},
      checkpoints={300,600,900},
      checktimes={10,10,10},
      checktime=20,
      fixed={   -- pos,sprite,w,h,scale,xpos,vflip
        all={
             {0,68,8,32,32,-1.2,false},{0,68,8,32,32,1.2,true},
             {50,66,16,16,32,1.4,false},{50,66,16,16,32,-1.4,false}
            },
        p3={{80,64,16,16,32,-1.4,true}},
        p4={{80,64,16,16,32,1.4,false}},
        p7={
          {20,68,8,32,32,-1.2,false},{20,68,8,32,32,1.2,true},
          {40,68,8,32,32,-1.2,false},{40,68,8,32,32,1.2,true},
          {60,68,8,32,32,-1.2,false},{60,68,8,32,32,1.2,true},
          {80,68,8,32,32,-1.2,false},{80,68,8,32,32,1.2,true},
        },
      },
      random={  -- vol,xoff,xrnd,sprite,w,h,scalebase,scalernd
        all={--{1,2,8,96,16,16,40,10},{1,2,8,96,16,16,40,10},
             {5,2,8,96,16,16,22,10},{5,2,8,70,16,32,24,10}},
        p2={{2,2,8,70,16,32,24,10}},
        p3={{2,2,8,70,16,32,24,10},{2,2,8,70,16,32,24,10},{2,2,8,70,16,32,24,10}},
        p4={{2,2,8,70,16,32,24,10}}
      }
		},
    {                       -- level 1
      name="dry desert",
			roadwidth=150,
			lanes=2,
			rumblelen=1,
			colors={road=5,finish=6,rumble={7,4},lanes={6,5},offroad=9,
              sky={12,12,12,12,13,13,1}},
      sun={x=99,y=24,r=6,col=10},
      plane2={s=130,w=16,h=16,y=-8,bg=1},
			track={
				{{100,0,0}},            --    len,[curve],[hill]
				{{80,-2}},   -- or lenin,lenmid,lenout,[curve],[hill]
				{{50,-4,1}},
				{{100,2,-1}},
				{{100,4,0}},
				{{100}},
				{{50,-4,-2}},
        {{40,0,3},{60,-2,-2}},
        {{40},{30,4,0}}
			},
      checkpoints={300,600,900},
      checktimes={10,10,10},
      checktime=20,
      fixed={   -- pos,sprite,w,h,scale,xpos,vflip
        all={
             {0,68,8,32,32,-1.2,false},{0,68,8,32,32,1.2,true},
             {50,98,16,16,32,1.4,false},{50,98,16,16,32,-1.4,false}
            },
        p3={{80,64,16,16,32,-1.4,true}},
        p4={{80,64,16,16,32,1.4,false}},
        p7={
          {20,68,8,32,32,-1.2,false},{20,68,8,32,32,1.2,true},
          {40,68,8,32,32,-1.2,false},{40,68,8,32,32,1.2,true},
          {60,68,8,32,32,-1.2,false},{60,68,8,32,32,1.2,true},
          {80,68,8,32,32,-1.2,false},{80,68,8,32,32,1.2,true},
        },
      },
      random={  -- vol,xoff,xrnd,sprite,w,h,scalebase,scalernd
        all={--{1,2,8,96,16,16,40,10},{1,2,8,96,16,16,40,10},
             {5,2,8,106,16,16,22,10},{5,2,8,72,16,32,24,10}},
        p2={{2,2,8,72,16,32,24,10}},
        p3={{2,2,8,72,16,32,24,10},{2,2,8,72,16,32,24,10},{2,2,8,72,16,32,24,10}},
        p4={{2,2,8,72,16,32,24,10}}
      }
		}
}

-- level properties
frame=0
gamestate=st_title
lap=1
lapstogo=0
laptime=0
racetime=0
laptimes={}
checktime=0
baseseg=0
playerseg=0
countdown=nil
showchecktime=true
showmessage=0
message=''
gamefailed=false

mmax=0
level=nil
checkindex=nil

plane1x=0
plane2x=0

particles={}

-------------------------------------------------------- utils
function tan(x)	return sin(x)/cos(x) end
function easein(a,b,p) return a + (b-a)*(p*p) end
function easeout(a,b,p) return a + (b-a)*(1-(1-p)*(1-p)) end
function easeinout(a,b,p)	return a + ((b-a)*(1-cos(p/2))) end
function percentrem(n,t) return (n%t)/t end
function interpolate(a,b,p) return a + (b-a)*p end

function lasty(segid)
	if(#segments==0) return 0
	last = segmentget(segid-1)
	if(last)return last.p2.world.y
	return 0
end

function segmentget(pos)
	if(pos>#segments)pos-=#segments
	return segments[pos]
end

function findsegment(pos,z)
	local offset = pos.z + z
		segid = pos.seg + flr(offset/seglength)
	return segmentget(segid)
end

function rumblewidth(rw)
	 return rw/8
end

function lanemarkerwidth(rw)
	 return rw/max(64, 8*lanes)
end

function project(p,cx,cy,cz,w,h,rw)
	p.camera.x = (p.world.x or 0) - cx
	p.camera.y = (p.world.y or 0) - cy
	p.camera.z = (p.world.z or 0) - cz
	p.screen.scale = camdep / ((p.camera.z*seglength)-position.z+1)
	p.screen.x = flr((w/2)+(p.screen.scale*p.camera.x*w/2))
	p.screen.y = flr((h/2)-(p.screen.scale*p.camera.y*h/2))
	p.screen.w = flr((p.screen.scale*rw*w)/2)
end

function adddecor(n,decor)
	add(segments[n].decor,{
    sprite=decor[2],
    sx=(decor[2]%16)*8,
    sy=flr(decor[2]/16)*8,
    width=decor[3],
    height=decor[4],
    scale=decor[5],
    offset=decor[6],
    fliph=decor[7]})
end

function remove100(tbl)
	local cnt=0
	for item in all(tbl) do
		if(cnt<100) then
			del(tbl,item)
		else
			item.p1.world.z-=100
			item.p2.world.z-=100
		end
		cnt+=1
	end
end

function lzero(num)
	if(num>9)return num
	return "0"..num
end

function formattime(laptime)
	local m = flr(laptime/600)
	local s = flr((laptime%600)/10)
	return lzero(m)..":"..lzero(s).."."..laptime%10
end

function getgear(speed)
	low=0 g=0
	for top in all(gears) do
		g+=1
		if speed<=top then
			ret={low=low,gear=g,top=top} break
		end
		low=top
	end
	return ret
end

function segdist(s1,s2)
  dist=s2-s1
  if(dist>0)return dist
  dist+=(#level.track*100)
  return dist
end

-------------------------------------------------------- init

function _init()
		-- make pink transparent not black
		palt(0,false)
		palt(14,true)

		setgamestate(st_title)
end

-------------------------------------------------------- update

function _update()
	if gamestate==st_race then
		updatetime()
    for car in all(cars) do
      movecar(car)
    end

		updateplayer()
    --if(frame%30==0)
	end
	if gamestate==st_title then
    if btnp(2) then
      menupos = (menupos+1)%2
    end
    if btnp(3) then
      menupos = (menupos-1)%2
    end

    if btnp(0) then
      if menupos==0 then
        ulevel = (ulevel-1)%#levels
      end
      if menupos==1 then
        ulaps = (ulaps-1)%10
      end
    end
    if (ulaps==0) ulaps=1

    if btnp(1) then
      if menupos==0 then
        ulevel = (ulevel+1)%#levels
      end
      if menupos==1 then
        ulaps = (ulaps+1)%10
      end
    end

		if btn(5) then
			countdown=6
			frame=0
      setlevel(ulevel+1)
      setgamestate(st_countdown)
      -- setgamestate(st_race) -----------------------------
      --message = 'testing something'
      --showmessage = 60
		end
	end
	if gamestate==st_countdown then
		updatecountdown()
	end
end

function setgamestate(state)
  gamestate = state
  if state==st_title then
    music(0)
  else
    music(-1)
  end

end

function updateenginesound()
  g = getgear(speed)
  pct = (speed-g.low)/(g.top-g.low)
  sfx(0,3,(6+g.gear*2)*pct+(g.gear-1)*2)
end

function updatecountdown()
	frame+=1
	if (frame%30==0) then
		countdown-=1
    if(countdown==1)sfx(1,2,16)
    if(countdown<5 and countdown>1)sfx(1,2,0)
		if countdown==0 then
			frame=0
			gamestate=st_race
		end
	end
end

function updatetime()
	frame+=1
	if frame==3 then
		frame=0
		laptime+=1
    racetime+=1
		if(laptime%10==0)checktime-=1
	end

	checktime=max(0,checktime)
  if(checktime==0 and not gamefailed) then
    gamefailed=true
    message = 'you failed...'
    showmessage = 90
  end
end

function passcheckpoint()
  checktime+=level.checktimes[checkindex]
  checkindex+=1
  message = 'checkpoint!'
  showmessage = 30
  if(checkindex>#level.checkpoints)checkindex=1
end

function passfinish()
  if(level.lapbest>laptime or level.lapbest==0)level.lapbest=laptime
  lap+=1
  if umode==0 and lapstogo>0 then
    lapstogo-=1
    if lapstogo==0 and not gamefailed then
      gamefailed=true
      message='race finished!'
      if(level.racebest>racetime or level.racebest==0) then
        level.racebest=racetime
        message='finished! best time!'
      else
        message='race finished!'
      end
      showmessage=90
    end
  end
  laptime=0
end

function updateplayer()

  speedprct=speed/maxspeed
	dx=0.1*speedprct
	position.z+=flr(speed)

	if position.z>=seglength then
    plane1x-=segments[position.seg].curve*0.2
    plane2x-=segments[position.seg].curve*0.6
    plane2x%=16
		position.seg+=flr(position.z/seglength)
		position.z%=seglength
		if (position.seg>#segments)position.seg-=#segments
		if (position.seg>100 and position.seg<190) then
			position.seg-=100
			position.chunk +=1
			remove100(segments)
			if(position.chunk==#level.track+1)position.chunk=1
			get_trackdata(position.chunk,2,1)
		end
	end
	playerseg = findsegment(position,player.z)

  if playerseg.id>=level.checkpoints[checkindex] and playerseg.id!=playerprev then
    passcheckpoint()
  end

  if playerseg.id<playerprev then
    passfinish()
  end
  playerprev=playerseg.id

	if not gamefailed then
    if(btn(0))player.x-=dx
  	if(btn(1))player.x+=dx
    player.x -= (dx * speedprct * playerseg.curve * centrifugal)
    player.x = max(min(player.x,1.2),-1.2)

    braking=0 accelerating=0
  	if btn(3) or btn(4) then
  		speed+=brake
  		braking=1
  	else
  		if btn(2) or btn(5) then
  			speed+=(accel/10)+(1-accel*speedprct)
  			accelerating=1
  		else
  			speed+=decel
  		end
  	end
  	if abs(player.x)>1  and speed>20 then
  		speed+=rumbledecel
      sfx(21)
  	end
  else speed+=rumbledecel end
	speed=max(0,min(speed,maxspeed))

  if (speed<1 and showmessage<1 and gamefailed) then
    setgamestate(st_title)
  end
end

function _draw()

	if gamestate==st_countdown or gamestate==st_race then
		-- draw sky
    rectfill(0,0,width,64,level.colors.sky[1])
    rectfill(0,64,width,height,level.colors.sky[#level.colors.sky])
    yoff = (maxy-64)/2 rowh=flr(64/#level.colors.sky)
    for r=1,#level.colors.sky do --#yoff,64+yoff,rowh do
		    rectfill(0,rowh*(r-1)+yoff,width,rowh*(r)+yoff,level.colors.sky[r])
    end
    -- draw sun
    if (level.sun) then
		    circfill(level.sun.x+plane1x,level.sun.y+yoff,level.sun.r,level.sun.col)
    end
    -- draw bg
    if level.plane2 then
      plane2y=maxy+level.plane2.y-((maxy-64)/4)
      for i=-level.plane2.w,128,level.plane2.w do
        spr(level.plane2.s,i+plane2x,plane2y,level.plane2.w,level.plane2.h)
      end
      rectfill(0,plane2y+level.plane2.h,width,height,level.plane2.bg)
    end

		-- draw road
		road_draw()

    if collided>0 then
      speed-=collided/3
      if(speed<0)speed=0
      addparticles(flr(collided/2))
      collided-=1
    end

		-- draw player
		spr(1,47,112,4,2)
		if braking==1 then
			spr(5,49,119)
			spr(5,68,119)
		end
		if accelerating==1 and speed<50 then
			spr(21,47-(speed%2),120,1,1)
			spr(21,71+(speed%2),120,1,1,true)
		end
	end

	if gamestate==st_countdown then
		showlamps()
	end

  for part in all(particles) do
    drawparticle(part)
  end


	if gamestate==st_race then
		-- draw hud
    updateenginesound()

		print("speed "..flr(speed),1,1,0)
    print("laps "..flr(lapstogo),104,1,0)
		--print("p.x"..player.x,1,10,0)
		print("laptime "..formattime(laptime),1,8,0)
    if(level.lapbest>0)print("bestlap "..formattime(level.lapbest),1,14,0)
		--print("seg "..baseseg.id.." "..playerseg.id.." "..position.seg,1,16,0)
		mem = stat(0)
		mmax = max(mem,mmax)
		--print("mem "..mem.." max "..mmax,1,36,0)

		-- draw checkpoint time
    if(showchecktime)bignum(46,20,lzero(checktime))

    -- draw message
    if showmessage>0 then
      showmessage-=1
      print(message,64-(#message*2),40,7+(flr(frame/2)%4))
    end


	end

	if gamestate==st_title then
		cls()
    print("bOCIANU presents",34,5,1)
    print("bOCIANU presents",33,4,12)
		spr(192,32,16,8,4)
    mc = 6
    if (menupos==0)mc=9
    print("level:",24,80,mc)
    print(levels[ulevel+1].name,54,80,mc)
    mc = 6
    if (menupos==1)mc=9
    print("laps:",24,90,mc)
    print(ulaps,54,90,mc)


		print("press x to start",29,112,7)
	end
end

function addparticles(n)
  pcol={0,0,6,8,9}
  for i=1,n do
    dx=rnd(32)-16
    dy=rnd(4)+1
    part={x=63+dx,y=122+dy,dx=dx/10,dy=-dy/2,s=1,l=5+flr(rnd(5)),c=pcol[flr(rnd(#pcol))+1]}
    add(particles,part)
  end
end

function drawparticle(part)
  circ(part.x,part.y,part.s,part.c)
  part.s+=0.15 part.x+=part.dx part.y+=part.dy part.l-=1
  if(part.l==0) del(particles,part)
end

-------------------------------------------------------- road

function addsegment(id,n,curve,y)
	local seg={
		id=id,curve=curve,
		decor={},
		p1={camera={},screen={},
				world={y=lasty(n),z=n}},
	  p2={camera={},screen={},
				world={y=y,z=n+1}}
	}
	segments[n]=seg
end

function addroad(id,si,e,h,l,curve,y)
	starty=lasty(si)
	endy=starty+(y*seglength)
	total = e+h+l
	for n=0,e-1 do
		addsegment(id,si,easein(0,curve,n/e),easeinout(starty,endy,n/total))
		id+=1 si+=1
	end
	for n=0,h-1 do
		addsegment(id,si,curve,easeinout(starty,endy,(e+n)/total))
		id+=1 si+=1
	end
	for n=0,l-1 do
		addsegment(id,si,easeout(curve,0,n/l),easeinout(starty,endy,(e+h+n)/total))
		id+=1 si+=1
	end
end

function get_trackdata(trackoffset,segoffset,blockcount)
	blockcount = blockcount or 1
	sid = ((trackoffset-1)*100)+1
	segoffset = ((segoffset-1)*100)+1
	for i=0,blockcount-1 do
    startoffset = segoffset
		block = level.track[trackoffset+i]
		for chunk in all(block) do
			if #chunk==1 then
				addroad(sid,segoffset,0,chunk[1],0,0,0)
				segoffset+=chunk[1] sid+=chunk[1]
			else
				if chunk[2]>9 then -- 3 lengts
					curve = chunk[4]
					hill = chunk[5] or 0
					addroad(sid,segoffset,chunk[1],chunk[2],chunk[3],curve,hill)
					segoffset+=chunk[1]+chunk[2]+chunk[3]
					sid+=chunk[1]+chunk[2]+chunk[3]
				else
					 curve = chunk[2]
					 hill = chunk[3] or 0
					 thrid = flr(chunk[1]/3)
					 addroad(sid,segoffset,thrid,thrid,chunk[1]-2*thrid,curve,hill)
					 segoffset+=chunk[1]
					 sid+=chunk[1]
				end
		  end
	  end
		rem = 100 - (segoffset-1)%100
		if rem!=100 then
			addroad(sid,segoffset,0,rem,0,0,0)
			segoffset+=rem
			sid+=rem
		end

    fixed = level.fixed
    if (fixed.all and #fixed.all!=0) then
      for decor in all(fixed.all) do
        adddecor(startoffset+decor[1],decor)
      end
    end
    idx='p'..(trackoffset+i)
    if (fixed[idx]) then
      for decor in all(fixed[idx]) do
        adddecor(startoffset+decor[1],decor)
      end
    end

    randoms = level.random
    if (randoms.all and #randoms.all!=0) then
      for decor in all(randoms.all) do
        for i=startoffset,startoffset+99,decor[1] do
      			x=rnd(decor[3])+decor[2]
      			if (rnd(2)<1) x*=-1
      			adddecor(i,{0,decor[4],decor[5],decor[6],decor[7]-rnd(decor[8]),x,(rnd(2)<1)})
      	end
      end
    end
    if (randoms[idx]) then
      for decor in all(randoms[idx]) do
        for i=startoffset,startoffset+99,decor[1] do
      			x=rnd(decor[3])+decor[2]
      			if (rnd(2)<1) x*=-1
      			adddecor(i,{0,decor[4],decor[5],decor[6],decor[7]-rnd(decor[8]),x,(rnd(2)<1)})
      	end
      end
    end
	end
end

function road_reset()
	segments={}
	get_trackdata(#level.track,1)
	get_trackdata(1,2)
end

function road_draw()
	baseseg = segmentget(position.seg)
	basepct = percentrem(position.z,seglength)
	playerseg = findsegment(position,player.z)
	playerpct = percentrem(position.z+player.z, seglength)
	player.y = interpolate(playerseg.p1.world.y,playerseg.p2.world.y,playerpct)
	maxy = height
	local x=0 local dx = -(baseseg.curve*basepct)
	for n=0,drawdist do
  	seg = segmentget(position.seg+n)
    seg.clip = maxy
		local cz = baseseg.p1.world.z
		project(seg.p1,(player.x*roadwidth)-x,player.y+camh,cz,width,height,roadwidth)
		project(seg.p2,(player.x*roadwidth)-x-dx,player.y+camh,cz,width,height,roadwidth)
		x += dx	dx += seg.curve
		if((seg.p2.screen.y<seg.p1.screen.y)	and	(seg.p2.screen.y<maxy)) then

			local color={
        road=level.colors.road,
        rumble=level.colors.rumble[seg.id%#level.colors.rumble+1],
        lanes=level.colors.lanes[seg.id%#level.colors.lanes+1],
        offroad=level.colors.offroad}
      if(seg.id==#level.track*100) color.road = level.colors.finish

			rendersegment(
			seg.p1.screen.x,seg.p1.screen.y,seg.p1.screen.w,
			seg.p2.screen.x,seg.p2.screen.y,seg.p2.screen.w,
			color)
			maxy = seg.p2.screen.y
		end
  end
	for n=drawdist,2,-1 do
		seg = segmentget(position.seg+n)
		for i=1,#seg.decor do
				decor = seg.decor[i]
				spritescale = seg.p1.screen.scale*roadwidth*width/2
				spritew = decor.width*spritescale/decor.scale
				spriteh = flr(decor.height*spritescale/decor.scale)
				spritex = seg.p1.screen.x + (decor.offset*spritescale) - spritew/2
				spritey = seg.p1.screen.y - (spriteh - 2)
				if spritey<height then
					clip(0,0,width,seg.clip)
					sspr(decor.sx,decor.sy,decor.width,decor.height,spritex,spritey,spritew,spriteh,decor.fliph)

				end
		end
	end
	clip()
  for car in all(cars) do
    cardist = segdist(baseseg.id,car.seg)
    if cardist>=1 and cardist<drawdist then
      seg = segmentget(position.seg+cardist)
      carprc = percentrem(car.z,seglength)
      spritescale = interpolate(seg.p1.screen.scale*width*0.8,seg.p2.screen.scale*width*0.8,carprc)
      spritew = 32*spritescale
      spriteh = 16*spritescale
      spritex = interpolate(seg.p1.screen.x,seg.p2.screen.x,carprc) + (car.x*spritescale*roadwidth) - flr(spritew/2)
      spritey = interpolate(seg.p1.screen.y,seg.p2.screen.y,carprc) - (spriteh - 1)
      if spritey<height then
        clip(0,0,width,seg.clip)
        sspr(car.sx,car.sy,32,16,spritex,spritey,spritew,spriteh)
      end
      if cardist<3 then
        if (spritex+spritew>48 and spritex<78) and (spritey+spriteh>112 and spritey<126) then
          collided+=10
          if(collided>20)collided=20
          if speed>car.spd then
            speed=car.spd-10
          else
            speed+=(car.spd-speed)/2
          end
          dx=48-spritex
          player.x+=dx*0.01
          sfx(20)
        end
      end
    end
  end
  clip()

end

function rendersegment(x1,y1,w1,x2,y2,w2,color)
	r1=rumblewidth(w1,lanes) r2=rumblewidth(w2,lanes)
	l1=lanemarkerwidth(w1, lanes) l2=lanemarkerwidth(w2, lanes)

	rectfill(0,y1,width,y2,color.offroad)

	render_poly({x1-w1-r1,y1,x1-w1,y1,x2-w2,y2,x2-w2-r2,y2}, color.rumble)
	render_poly({x1+w1+r1,y1,x1+w1+1,y1,x2+w2,y2,x2+w2+r2+1, y2}, color.rumble)
	render_poly({x1-w1,y1,x1+w1,y1,x2+w2,y2,x2-w2,y2},color.road)
	if (color.lanes!=0 and l2>0.05) then
	      lanew1 = w1*2/lanes lanew2 = w2*2/lanes
	      lanex1 = x1 - w1 + lanew1	lanex2 = x2 - w2 + lanew2
	      for lane=1,lanes-1 do
					 render_poly({lanex1 - l1/2, y1, lanex1 + l1/2, y1, lanex2 + l2/2, y2, lanex2 - l2/2, y2 }, color.lanes)
					 lanex1 += lanew1
					 lanex2 += lanew2
	    	end
	end
end

---------------------------------------------- cars

function addcar(x,z,seg,speed,s,tactic)
  car={sx=(s%16)*8,sy=flr(s/16)*8,x=x,z=z,seg=seg,spd=speed,t=type}
  add(cars,car)
end

function movecar(car)
  --speedprct=car.spd/maxspeed
  --dx=0.1*speedprct
  car.z+=flr(car.spd)
  fulllen=100*#level.track
  if car.z>=seglength then
    car.seg+=flr(car.z/seglength)
    car.z%=seglength
    if(car.seg>fulllen)car.seg-=fulllen
  end
end

---------------------------------------------- level
function setlevel(lvl)
  level=levels[lvl]
  roadwidth=level.roadwidth
  lanes=level.lanes or 3
  laptime=0
  racetime=0
  laptimes={}
  checktime=level.checktime or 90
  checkindex=1
	level.laps=level.laps or 3
	level.lapbest=level.lapbest or 0
  level.racebest=level.racebest or 0
  lapstogo=ulaps
  lap=1
  player.x=0
  player.z=400
  position.seg=99
  position.z=0
  position.chunk=1
  speed=0
  collided=0
  gamefailed=false
	road_reset()

  cars={}
  for i=30,800,20 do
    carsprites={1,6,10,33}
    addcar(rnd(1)-0.5,0,i,100+flr(rnd(80)),carsprites[flr(rnd(#carsprites))+1])
  end

end

function bignum(x,y,txt)
	txt=""..txt
	for i=1,#txt do
		local c=sub(txt,i,i)
		spr(37+c,x+(i*9),y,1,2)
	end
end

function showlamps()
	lamps={14,14,14,14}
	if countdown>1 then
			print("get ready!",45,13,0)
			print("get ready!",44,12,7+(flr(frame/2)%4))
	else
		print("go!",61,13,0)
		print("go!",60,12,10)
	end
	if(countdown==4)lamps={15,14,14,14}
	if(countdown==3)lamps={15,15,14,14}
	if(countdown==2)lamps={15,15,15,14}
	if(countdown==1)lamps={15,15,15,30}
	for i=1,#lamps do
		spr(lamps[i],34+i*10,24)
	end
end


-- draws a filled convex polygon
-- v is an array of vertices
-- {x1, y1, x2, y2} etc
-- stolen from http://www.lexaloffle.com/bbs/?pid=34264&tid=28312
function render_poly(v,col)
	col=col or 5
	local x1,x2={},{}
	for y=0,127 do
		x1[y],x2[y]=128,-1
	end
	local y1,y2=128,-1
	for i=1, #v/2 do
		local next=i+1
		if (next>#v/2) next=1
		local vx1=flr(v[i*2-1])
		local vy1=flr(v[i*2])
		local vx2=flr(v[next*2-1])
		local vy2=flr(v[next*2])
		if vy1>vy2 then
			local tempx,tempy=vx1,vy1
			vx1,vy1=vx2,vy2
			vx2,vy2=tempx,tempy
		end
		if vy1~=vy2 and vy1<128 and
		vy2>=0 then
			if vy1<0 then
				vx1=(0-vy1)*(vx2-vx1)/(vy2-vy1)+vx1
				vy1=0
			end
			if vy2>127 then
				vx2=(127-vy1)*(vx2-vx1)/(vy2-vy1)+vx1
				vy2=127
			end
			for y=vy1,vy2 do
				if (y<y1) y1=y
				if (y>y2) y2=y
				x=(y-vy1)*(vx2-vx1)/(vy2-vy1)+vx1
				if (x<x1[y]) x1[y]=x
				if (x>x2[y]) x2[y]=x
			end
		end
	end
	for y=y1,y2 do
		local sx1=flr(max(0,x1[y]))
		local sx2=flr(min(127,x2[y]))
		local c=col*16+col
		local ofs1=flr((sx1+1)/2)
		local ofs2=flr((sx2+1)/2)
		memset(0x6000+(y*64)+ofs1,c,ofs2-ofs1)
		pset(sx1,y,c)
		pset(sx2,y,c)
	end
end


__gfx__
00000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeefefeffeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee0000eeee0000ee
00000000eeeeeeee9aaaaaaaaaaaaaa9eeeeeeeee288f82eeeeeeeeedccccccccccccccdeeeeeeeeeeeeeeee2888888888888882eeeeeeeee0d5550ee0a9980e
00000000eeeeeee9aa999999999999aa9eeeeeeee288f82eeeeeeeedccddddddddddddccdeeeeeeeeeeeeee288222222222222882eeeeeee0d5555500a988880
00000000eeee99aa9dddddddddddddd9aa99eeeeefefeffeeeeeddccd55555555555555dccddeeeeeeee22882dddddddddddddd28822eeee0555551009888840
00000000eee9a1000000000000000000001a9eeeeeeeeeeeeeedc1000000000000000000001cdeeeeee28100000000000000000000182eee0555551009888840
00000000ee994991dddddddddddddddd199499eeeeeeeeeeeedd1dd155555555555555551dd1ddeeee224221dddddddddddddddd122422ee0555510008888420
00000000e9949aa994444444444444499aa9499eeeeeeeeeedd1dccdd11111111111111ddccd1ddee224288224444444444444422882422ee051100ee084420e
00000000e99aaaaaaaaaaaaaaaaaaaaaaaaaa99eeeeeeeeeeddccccccccccccccccccccccccccddee228888888888888888888888888822eee0000eeee0000ee
eee77eeee980227221116666666611122722089eeeeeeeeeed8022722111666666661112272208dee280227221116666666611122722082eee0000ee00770077
eee770eee980227221111111111111122722089eeeeeeeeeed8022722111111111111112272208dee280227221111111111111122722082ee07aab0e00770077
eeee00eee9aaaaaaaaaaaaaaaaaaaaaaaaaaaa9eeeeeeeeeedccccccccccccccccccccccccccccdee288888888888888888888888888882e07abbbb077007700
eeeeeeeee999aaaaaaaaaaaaaaaaaaaa6aaa999e7eeeeeeeedddcccccccccccccccccccc6cccdddee222888888888888888888886888222e0abbbb3077007700
eeeeeeeee009900009000000000000965609900ee7e7eeeee00dd0000d000000000000d6560dd00ee002200002000000000000265602200e0abbbb3000770077
eee77eeee004999999999999999999996999400e7e6e6e7ee001dddddddddddddddddddd6ddd100ee004222222222222222222226222400e0bbbb35000770077
eee770eee000000eeeeeeeeeeeeeeeeee000000ee6e6e6e6e000000eeeeeeeeeeeeeeeeee000000ee000000eeeeeeeeeeeeeeeeee000000ee0b3350e77007700
eeee00eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee0000ee77007700
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeee3bbbbbbbbbbbbbb3eeeeeeeee77777eeeee77eeee77777ee7777777eeeeee77e7777777ee77777ee7777777ee77777eee77777eee77777ee
eeeeeeeeeeeeeee3bb333333333333bb3eeeeeee7777777eee7770ee7777777e77777770eeee7770777777707777777e777777707777777e7777777e7777777e
eeeeeeeeeeee33bb3dddddddddddddd3bb33eeee77000770e77770ee77000770e0000770eee77770770000007700077077000770770007707700077077000770
eee77eeeeee3b1000000000000000000001b3eeeaa0eeaa0ee0aa0eeaa0eeaa0eeeeaaa0eeaaaaa0aa0eeeeeaa0ee770e00eeaa0aa0eeaa0aa0eeaa0aa0eeaa0
eee770eeee331331dddddddddddddddd133133ee770ee770eee770eee00ee770eee77700e7770770770eeeee770eee00eeee7770770ee770770ee770770ee770
eeee70eee3313bb331111111111111133bb3133eaa0eeaa0eeeaa0eeeeeeaaa0eeaaa00eaaa00aa0aaaaaaeeaaaaaaeeeeeaaa00eaaaaa0eaaaaaaa0aa0eeaa0
eeeee0eee33bbbbbbbbbbbbbbbbbbbbbbbbbb33eaa0eeaa0eeeaa0eeeeeaaa00eeeaaaeeaa00eaa0aaaaaaaeaaaaaaaeeeaaa00eaaaaaaaeeaaaaaa0aa0eeaa0
eeeeeeeee380227221116666666611122722083e990ee990eee990eeee99900eeeee999e990ee990e000099099000990ee9900ee99000990ee000990990ee990
eeeeeeeee380227221111111111111122722083eaa0eeaa0eeeaa0eeeaaa00eeeeeeeaa0aaaaaaa0eeeeeaa0aa0eeaa0eeaa0eeeaa0eeaa0eeeeeaa0aa0eeaa0
eeeeeeeee3bbbbbbbbbbbbbbbbbbbbbbbbbbbb3e990ee990eee990ee99900eee99eee9909999999099eee990990ee990ee990eee990ee99099eee990990ee990
eeeeeeeee333bbbbbbbbbbbbbbbbbbbb6bbb333e990ee990eee990ee9900eeee990ee990e0000990990ee990990ee990ee990eee990ee990990ee990990ee990
eee77eeee003300003000000000000365603300e99999990e999999e9999999e99999990eeeee9909999999099999990ee990eee999999909999999099999990
eee770eee001333333333333333333336333100ee4444400e444444044444440e4444400eeeee440e4444400e4444400ee440eeee4444400e4444400e444440e
eeee00eee000000eeeeeeeeeeeeeeeeee000000eee00000eee000000e0000000ee00000eeeeeee00ee00000eee00000eeee00eeeee00000eee00000eee0000ee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
77777777777777771111111111111111eee66dddeee77eeeeeeeeebbeeeeeeeeeeeeeeeeeeeeeeee777777777777777777777777777777777777777777777977
77777777777777771111171771711111ee6dd555eee88eeeeeeeebbbbeeeeeeeeeeeeeeeeeeeeeee000007700077077707700007770007707077707000007e77
77777877777777771111171771711111e6d5eeeeeee77eeeeeeeebbbbbbeeeeeeeeeeeeeeeeeeeee077777077707077077707770707770707007707770777e77
777788777777777711111717717111116d5eeeeeeee67eeeeeeebbbbbbeeeeeeeeeeeeeeeeeeeeee077777077777070777707770707770707070707770777e77
77788888888777771111d717717d11116d5eeeeeeee67eeeeeebbbb3bbbeeeeeeeeeeeeeeeeeeeee000777077777007777700007707770707077007770777e77
7788888888887777111d77177177d1116d5eeeeeeee67eeeeeebbbb3bebeeeeeeeeee33eeeeeeeee077777077777070777707777707770707077707770777e88
777888888888877711777d1771d777116d5eeeeeeee67eeeeeeebbe34bbeeeeeeeee333eeeeeeeee077777077707077077707777707770707077707770777e88
777788777888877711777117711777116d5eeeeeeee67eeeeeebbbbe3bbbbeeeeeee3333eeeeeeee000007700077077707707777770007707077707770777e88
777778777788877711111111111111116d5eeeeeaa0000aaeeebbbbb3b3ebeeeeeee3433eeeeeeee777777777777777777777777777777777777777777777988
777777777788877711717771717711716d5eeeeeaaa0000aeebbbbbb333beeeeeeee3333eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
777777777788877717d71717d77d71716d5eeeeeaaaa0000eeeb3bb443beeeeeeeee3333eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
7777777777888777177717177777d1716d5eeeee0aaaa000eeeb3be4e3beeeeeeeee3343eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
777777777777777717171717177d71716d5eeeee00aaaa00eeeeb3b43bebbeeeeeee3333eee3eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
777777777777777711111111111111116d5eeeee000aaaa0eeebbb34bbbbebbe33ee3333ee333eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
ee444eeeeee444eeeeeeee4555eeeeee6d5eeeee0000aaaaeeeebbbbbbbbbebe33ee3433ee433eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
ee444eeeeee444eeeeeeee4555eeeeee6d5eeeeea0000aaaeeeebbbbbbb3bbbe433e3333ee333eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee6d5eeeeeee888eeeebbbebb4bb33ebee33333333ee334eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
eeeeeeebbbbbbeeeeeeeeee00eeeeeee6d5eeeeee88f88eeebe33bb4e33ebeeee3334333ee333eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
eeeebbbbbbbbbbeeeeeeee0aa0eeeeee6d5eeeeee88888eeeebb33b4b3bbebeeee333333ee343eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
eebbbbbbb33bbbbeeeeee09aa90eeeee6d5eeeeeee888eeeeeebb334bbbebbeeeeee3333ee333eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
eebbbbbb33bb3bbeeeeee0a99a0eeeee6d5eeeeeebebeeeebbebbbe3bbbbbbbbeeee3334e3343eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
eebbbbb3bb333bbeeeee0aa00aa0eeee6d5eeeeeebbbebeebbbbbbb4bbbb3ebeeeee343333433eeeeeeeeeaaaaaeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
ebb3bbbbbbb443beeee09aa00aa90eee6d5eeeeeeebbbbeebbb3bbb4bbb3bbeeeeee33333333eeeeeeeaaa9a9a9aeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
e4b3bbbbbbb433beeee0aaa00aaa0eee6d5eeeeeeeebbeeeebb33b4eb33beeeeeeee3333343eeeeeeea9a9a9a9a9aeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
e4b33b34bbb33bbeee09aaa44aaa90ee6d5eeeeeeeaaaaeeeebe3bbe33bbeeeeeeee3433eeeeeeeeee9a999999999eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
e443bb34bb34bbbeee0aaaaaaaaaa0ee6d5eeeeeeaa99aaeeeebb3bbbbbeeeeeeeee3333eeeeeeeeeaa99999999999eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
eb444b334b4bbb4ee0aaaaa00aaaaa0e6d5eeeeeeaa99aaeeeeeeb44beeeeeeeeeee3333eeeeeeeee9999999999999eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77
eebb44b3344b344ee09aaaaaaaaaa90e6d5eeeeeeaaaaaaeeeeeeeb4eeeeeeeeeeee3334eeeeeeeee494999999949eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
eebbbb43444444eeee000000000000ee6d5eeeeeeeaaaaeeeeeeeee4eeeeeeeeeeee3333eeeeeeeeee44494949494eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
eeeeebb4444bbbeeee54ee4554ee45ee6d5eeeeeeeebeeeeeeeeeee4eeeeeeeeeeee3333eeeeeeeeeee944444444eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
eeeeeeee4bbbeeeee544eeeeeeee445e6d5eeeeebbebebbbeeeeee444eeeeeeeeeee3333eeeeeeeee4444444444444eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
eeeeeeeeeeeeeeeee54eeeeeeeeee45e6d5eeeeeebbbbbeeeeeeee444eeeeeeeeeee3333eeeeeeeeee44444444444eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
eeebebebebeeeeee99eeeeeeeeeeeee9eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
ebb3b3b3b3bbebeb94999eeeaaaeee94eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
b33333333333b3b34949499a999ae949eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
3334343434333333949494a99999a494eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
43434343434343434444aa9999999a44eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
343434343434343444aa99a9999999a4eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
4344434443444344aa99999a9999999aeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
444344434443444399999999aa999999eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
44444444444444449999999999aa9999eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
434443444344434499a999999999a999eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
4444444444444444999aa99999999a99eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
444544454445444599999a99999999a9eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
4544454445444544999999aa9999999aeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
545454545454545499999999aa999999eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
5555555555555555a999999999999999eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
55555555555555559a99999999999999eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000ccccccccc0000000000000000000000000000000cccc00000000000000000000000000000000000000000000000000000000000000000000000
0000000000cccc1111111ccc0000000000000000000000000000cc11c00000000000000000000000000000000000000000000000000000000000000000000000
00000000ccc111111111111cc000000000000000000000000000c111c00000000000000000000000000000000000000000000000000000000000000000000000
00000000c111cc11cccccc11cc00000000000000000000000000c111c00000000000000000000000000000000000000000000000000000000000000000000000
00000000ccccc111c0000cc11c0000000000000000000000000cc11cc00000000000000000000000000000000000000000000000000000000000000000000000
00000000000cc11cc0000cc11c0000000000000000000000000c11cc000000000000000000000000000000000000000000000000000000000000000000000000
00000000000c111c00cccc11cc0000000000000000000000000c11c0000000000000000000000000000000000000000000000000000000000000000000000000
0000000000cc11ccccc1111cc0000000000ccccccc000000000c11c0000000000000000000000000000000000000000000000000000000000000000000000000
0000000000c111cc11111ccc00ccccc00ccc11111c0000cccccc1cc0000000000000000000000000000000000000000000000000000000000000000000000000
000000000cc11c1111cccc000cc111cc0c1111cc1cc0ccc1111c1c00000000000000000000000000000000000000000000000000000000000000000000000000
000000000c11111111cc0000cc11c11cc1111ccc11ccc1111c111c00000000000000000000000000000000000000000000000000000000000000000000000000
00000000cc111ccc111cc00cc11ccc11ccccccc111cc1111cccc1c00000000000000000000000000000000000000000000000000000000000000000000000000
00000000c111cc0cc111cc0c11cc0c11c0ccc111c11c111cc00c1c00000000000000000000000000000000000000000000000000000000000000000000000000
00000000c111c000cc111ccc1cc00c11ccc111ccc11c11cc00cc1c00000000000000000000000000000000000000000000000000000000000000000000000000
00000000c11cc0000cc1111c1cc0cc1ccc111cccc1cc11cc0cc11c00000000000000000000000000000000000000000000000000000000000000000000000000
000000ccc11cccccc0cc111111ccc11c0c11ccc111ccc11ccc11cc00000000000000000000000000000000000000000000000000000000000000000000000000
0000ccc111111111cccccc11c11111cc0cc111111cc0cc1111111c00000000000000000000000000000000000000000000000000000000000000000000000000
0000c1111111111111c00cccccccccc000cccccccc000ccccccccc00000000000000000000000000000000000000000000000000000000000000000000000000
000cc1c111ccccc111c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000c11c11cc000ccccc0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000cc1111c000000000cccc00cccc0000ccc000cccccc000cccc00ccccc000000000000000000000000000000000000000000000000000000000000000000000
0000c1111ccccccccccc11cc0c11c0000c1ccccc1111cc0cc11cccc111ccc0000000000000000000000000000000000000000000000000000000000000000000
0000cc1111111111cc11c11c0c11c00cc111cc111cc11ccc1111cc11cc11c0000000000000000000000000000000000000000000000000000000000000000000
00000c11111111ccc11cc11c0cc1cc0c11c1111cccc11cc1cc1111ccccc1c0000000000000000000000000000000000000000000000000000000000000000000
00000c11cccccccc11cc11cc0cc11c0c1ccc111cc111ccc1cc111cc000c1c0000000000000000000000000000000000000000000000000000000000000000000
0000cc11c000000c1111ccc0cc111ccc1c0cc11111cccc1ccc11cc0000ccc0000000000000000000000000000000000000000000000000000000000000000000
0000c11cc000000c111cc00cc11cc1c11c00cc11ccc00c1cc11cc000000000000000000000000000000000000000000000000000000000000000000000000000
0000c11c0000000cc11ccccc11ccc111cc000c111ccccc1cc11c0000000000000000000000000000000000000000000000000000000000000000000000000000
000cc11c000000000c1111111cc0cc11c0000cc1111111ccc1cc0000000000000000000000000000000000000000000000000000000000000000000000000000
000c11cc000000000ccc111ccc000cccc00000ccc1111cc0ccc00000000000000000000000000000000000000000000000000000000000000000000000000000
000cccc000000000000ccccc0000000000000000cccccc0000000000000000000000000000000000000000000000000000000000000000000000000000000000

__gff__
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__map__
0055555555550000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0055555555550000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
01200000004100141002410034100441005410064100741008410094100a4100b4100c4100d4100e4100f410104101141012410134101441015410164101741018410194101a4101b4101c4101d4101e4101f410
001b00002477005700017000270002700057000770007700067000670008700027000170000700007000070029770297722977129702007000070000700007000070000700007000070000700007000070000700
0110000021545215452154521545005052d5312d522005052154521545215452154500505005050050500505215451f5451f5451f545005052b5312b54200505215451f5451f5451f54500505005050050500505
011000000975020700157500070009750007001575000700097500070015750007000975000700157500070007750007001375000700077500070013750007000775000700137500070007750007001375000700
0110000021545215452154521545005052d5052d505005052154521545215452154500505005050050500505215451f5451f5451f545005052b5052b50500505215451f5451f5451f54500505005050050500505
01080020150730000018615000002b655000001861500000150730000018615000002b635000001503315053150730000018615000002b655000003c61500000150730000018615000002b6352b6053062400000
011000000575005405117500070005750007001175000700057500070011750007000575000700117500070002750007000e75000700027500e4050e750007000475000700107500070004750007001075000700
0110000034545345453454534545005052d5052d50500505345453454534545345450050500505005050050534545325453254532545005052b5052b505005053454532545325453254500505005050050500505
0110000015200092000920000200002000020000200002000020000200152301523518230182351a2301a2351c2301c2301c235002001d2301d2351c2311c2301c2321c2351a2341a2301a235002001823418230
0110000018230182350020000200002000020000200002000020000200152301523518230182351a2301a23500200002001c2301c2351a2301a23518230182351a2311a2301a2321a23515230152321523215235
01080020150730000018615000002b655000001861500000150730000018615000002b635000001503315053150730000018615000002b655000003c6150000015073150032163324625306152b6061503300000
0110000018230182350020000200002000020000200002000020000200152301523518230182351a2301a23500200002001c2301c2351f2301f23521230212352423124230212322123523230232322123221235
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0006000031660236501f6401d64019632166301462213620241122411224112241122411223115006000060000600006000060000600006000060000600006000060000600006000060000600006000060000600
01030000006100c630006100c63004600036000360003600026000260004600036000360000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__music__
00 03040548
00 06020a49
00 03040508
00 06020509
00 03040508
00 0602050b
00 03040507
02 06040a07
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
